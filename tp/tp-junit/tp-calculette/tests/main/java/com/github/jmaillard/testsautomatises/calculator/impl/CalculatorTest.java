package main.java.com.github.jmaillard.testsautomatises.calculator.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTest {

	private Calculator calculator = new Calculator();
	private int a;
	private int b;

	@Test
	public void add_2_to_3_get_5() {
		//GIVEN
		b = 2;
		a = 3;

		//WHEN & THEN
		assertEquals(5, calculator.sum(a, b));
	}

	@Test
	public void add_2_to_minus3_get_minus1() {
		//GIVEN
		b = 2;
		a = -3;
		
		//WHEN & THEN
		assertEquals(-1, calculator.sum(a, b));
	}

	@Test
	public void substract_3_to_2_get_minus1() {
		//GIVEN
		b = 3;
		a = 2;
		
		//WHEN & THEN
		assertEquals(-1, calculator.minus(a, b));
	}

	@Test
	public void substract_minus3_to_2_get_5() {
		//GIVEN
		a = 2;
		b = -3;
		
		//WHEN & THEN
		assertEquals(5, calculator.minus(a, b));
	}

	@Test
	public void divide_10_by_2_get_5() {
		//GIVEN
		a = 10;
		b = 2;
		
		//WHEN & THEN
		assertEquals(5, calculator.divide(a, b));
	}

	@Test (expected = ArithmeticException.class)
	public void divide_10_by_0_get_failure() {
		//GIVEN
		b = 0;
		a = 10;
		
		calculator.divide(a, b);
	}

	@Test
	public void multiply_2_to_3_get_6() {
		//GIVEN
		b = 2;
		a = 3;
		
		//WHEN & THEN
		assertEquals(6, calculator.multiply(a, b));
	}

	@Test
	public void multiply_2_to_0_get_0() {
		//GIVEN
		b = 2;
		a = 0;

		//WHEN & THEN
		assertEquals(0, calculator.multiply(a, b));
	}

}

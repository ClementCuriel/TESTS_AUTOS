package sqli.df.tp.tp_test;

import static org.junit.Assert.assertEquals;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TpStep {

	private TpTest tp = new TpTest();

	@Given("^there are (\\d+) places$")
	public void there_are_places(int places) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		tp.setBarPlace(places);
	}

	@Given("^the cocktail cost (\\d+) euros$")
	public void the_cocktail_cost_euros(int cocktailPrice) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		tp.setCocktailPrice(cocktailPrice);
	}

	@Given("^there are (\\d+) occupied places$")
	public void there_are_occupied_places(int occupiedPlace) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		tp.setOccupiedPlace(occupiedPlace);
	}

	@When("^I enter with (\\d+) people$")
	public void i_enter_with_people(int people) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		tp.setPeople(people);
	}

	@Then("^the bar (\\d+)$")
	public void the_bar(int open) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		assertEquals(open, tp.calculOpenBar());
	}

	@Given("^they are (\\d+) people$")
	public void they_are_people(int people) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		tp.setPeople(people);
	}

	// NEW
	@Given("^they tooks (\\d+) cocktails$")
	public void they_tooks_cocktails(int nbCocktail) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		tp.setNbCocktail(nbCocktail);

	}

	@Given("^\"([^\"]*)\" pay the totality$")
	public void pay_the_totality(String person) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		tp.setPersonPay(person);
	}
	
	@Given("^they have already taken (\\d+) cocktails$")
	public void they_have_already_taken_cocktails(int alreadyBeer) throws Throwable {
	   tp.setAlreadyTookBeer(alreadyBeer);
	}
	
	@Then("^Mr Pignon is (\\d+)$")
	public void mr_Pignon_is(int pignonIsHappy) throws Throwable {
		assertEquals(pignonIsHappy, tp.pignonIsHappy());
	}

}
